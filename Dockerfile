FROM gliderlabs/herokuish as builder
COPY . /tmp/app
ARG BUILDPACK_URL
ENV USER=herokuishuser
RUN /bin/herokuish buildpack build

FROM gliderlabs/herokuish
COPY --chown=herokuishuser:herokuishuser --from=builder /app /app
ENV PATH $PATH:/app/.heroku/php/bin
RUN cd /app/ && composer run-script post-root-package-install
ENV PORT=5000

ARG APP_URL
ARG DB_DATABASE
ARG APP_ENV
ARG APP_DEBUG

ENV USER=herokuishuser
ENV APP_URL ${APP_URL}
ENV DB_DATABASE ${DB_DATABASE}
ENV APP_ENV ${APP_ENV}
ENV APP_DEBUG ${APP_DEBUG}
ENV APP_SECURE "true"
EXPOSE 5000

CMD ["php", "/app/artisan", "migrate:fresh", "--seed"]
CMD ["/bin/herokuish", "procfile", "start", "web"]
